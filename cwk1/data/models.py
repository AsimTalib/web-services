from django.db import models

# professor details
class Professor(models.Model):
    RESEARCH_AREA = (
    ('CS','Computer Science'),('AI','Artifcal Intelligence'),('DATA','Data')
    )
    #Unique code of professor
    code=models.CharField(max_length= 10,unique=True)
    #name of professor
    name = models.CharField(max_length= 100)
    #research area
    research_area = models.CharField(max_length = 100,choices = RESEARCH_AREA,default='CS')

    # Overriding the default tostring method
    def __str__(self):
            return "%s %s" % (self.name,self.code)

##information about a modules
class Module(models.Model):
    CREDITS_VALUE = (
    (10,10),(15,15),(20,20)
    )
    #unique code of a module
    code = models.CharField(max_length=10,unique=True)
    #module name
    module_name = models.CharField(max_length=100,unique=True)
    #credit amounts
    credits = models.IntegerField(choices = CREDITS_VALUE)

    # Overriding the default tostring method
    def __str__(self):
            return "%s" % (self.module_name)

#instance of a model
class ModuleInstance(models.Model):
    #Year choices
    YEAR = (
        (2017,2017),(2018,2018),(2019,2019),(2020,2020)
    )
    #semester choices
    SEMESTER_VALUES = (
            (1,1),(2,2),
         )
    #module data foreign key as it is from the module table
    module = models.ForeignKey(Module,on_delete = models.CASCADE)
    #professor has many to many , many professors can teach many modules
    professor = models.ManyToManyField(Professor)
    #teaching semster
    year = models.IntegerField(choices = YEAR)
    #teaching semester
    semester = models.IntegerField(choices = SEMESTER_VALUES)

    #overriding the string
    def __str__(self):
        return "%s" % (self.module)

#Ratings of a professor
class Rating(models.Model):
    SCALE_VALUES = (
        (1,1),(2,2),(3,3),(4,4),(5,5),
    )
    #foreign key from the module instance , each professor rated off a certain module instance as
    #an avergae can be found later on based on a module instance ( option 4)
    moduleInstance = models.ForeignKey(ModuleInstance,on_delete=models.CASCADE,related_name = 'topic',null=True)
    #foreign key from the professor tables as unable to get the professors from the module instances as only one foreign key
    #between a table , this allows us to see the professors and we can filter the professors based off the professors in the
    #module instance
    
    professor = models.ForeignKey(Professor,on_delete=models.CASCADE,related_name = 'lecturer',null= True)
    rating = models.IntegerField(choices =SCALE_VALUES)

    def __str__(self):
         return "%s,%s" %(self.moduleInstance,self.professor)
