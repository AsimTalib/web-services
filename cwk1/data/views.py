from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import *
from django.http import HttpResponse , HttpResponseBadRequest
import json
from django.contrib.auth.models import User
import re
import math
from django.contrib import auth


#  moduleInstance = models.ForeignKey(ModuleInstance,on_delete=models.CASCADE,related_name = 'topic',null=True)
#     professor = models.ForeignKey(Professor,on_delete=models.CASCADE,related_name = 'lecturer',null= True)
#     rating = models.IntegerField(choices =SCALE_VALUES)

#  module = models.ForeignKey(Module,on_delete = models.CASCADE)
#     professor = models.ManyToManyField(Professor)
#     year = models.IntegerField(choices = YEAR)
#     semester = models.IntegerField(choices = SEMESTER_VALUES)


@csrf_exempt
def rate(request):
    if(request.method != 'POST'):
        return HttpResponseBadRequest('Only POST requests allowed for this resource\n', content_type='text/plain')

    pid = request.POST['pid']
    mcode = request.POST['mcode']
    year = request.POST['year']
    semester = request.POST['semester']
    rating = request.POST['rating']

    if request.user.is_authenticated:
        minstance=ModuleInstance.objects.get(module=Module.objects.get(code=mcode),year = year,semester = semester)
        professor = Professor.objects.get(code=pid)
        #rate JE1 CD1 2017 1 2
        #check professor is in module instance lecturers
        rating = Rating(moduleInstance=minstance, professor=professor, rating=rating)
        rating.save()
        return HttpResponse(('New rating successfully added'), status=201, content_type='text/plain')
    else:
        return HttpResponse(("Error! Failed to do rating, maybe not logged in"), status=404, content_type='text/plain')




@csrf_exempt
def register(request):
    if(request.method != 'POST'):
        return HttpResponseBadRequest('Only POST requests allowed for this resource\n', content_type='text/plain')

    username = request.POST['username']
    password = request.POST['password']
    email = request.POST['email']

    user = auth.authenticate(request, username=username, password=password)
    try:
        if user is not None:    #user already exists
                return HttpResponse('Error! User already exists!', status=404, content_type='text/plain')
        else:
            user = User.objects.create_user(username=username,email=email,password=password)
            return HttpResponse(("New user "+username+" succesfully created but not logged in"), status=200, content_type='text/plain')
    except Exception:
        return HttpResponse(("Error! Failed to create new user: "+username), status=503, content_type='text/plain')



@csrf_exempt
def logout(request):
    if(request.method != 'POST'):
        return HttpResponseBadRequest('Only POST requests allowed for this resource\n', content_type='text/plain')

    try:
        auth.logout(request)
    except Exception:
        return HttpResponse(("Error! Logout Failed"), status=503, content_type='text/plain')

    return HttpResponse(("Logout Succesfful"), status=200, content_type='text/plain')



@csrf_exempt
def login(request):
    if(request.method != 'POST'):
        return HttpResponseBadRequest('Only POST requests allowed for this resource\n', content_type='text/plain')

    username = request.POST['username']
    password = request.POST['password']

    user = auth.authenticate(request, username=username, password=password)
    if user is not None:
        if user.is_active:
            auth.login(request, user)
            return HttpResponse(("Welcome %s Login Successfull." % user.username), status=200, content_type='text/plain')
        else:
            return HttpResponse('Error! Account is Disabled!')
    else:
        return HttpResponse(("Error! Authentication failed, user may not exist"), status=401, content_type='text/plain')


def getStars(avg):
        avg = math.ceil(avg)
        stars = ""
        for i in range(avg):
            stars += "*"

        return stars

@csrf_exempt
def getaverage(request):
    profId = request.GET['profId']
    moduleCode = request.GET['moduleCode']

    query = Rating.objects.all()
    query = query.values("moduleInstance", "professor", "rating")

    profname=""
    modulename=""
    avg=0
    count=0

    if(len(query) > 0):
        for r in query:
            pcode = str(Professor.objects.get(id=r["professor"]).code)
            mcode = str(ModuleInstance.objects.get(id=r["moduleInstance"]).module.code)
            if(profId==pcode and moduleCode==mcode):
                profname = str(Professor.objects.get(id=r["professor"]).name) + " ("+pcode+")"
                modulename = str(ModuleInstance.objects.get(id=r["moduleInstance"]).module.module_name) + " ("+str(ModuleInstance.objects.get(id=r["moduleInstance"]).module.code)+")"
                avg=avg+int(r["rating"])
                count=count+1

        if(count==0):
            return HttpResponse("Error! No ratings for profID: "+profId+" moduleCode: "+moduleCode, status=404)

        res="The rating of Professor "+profname+" in module "+modulename+" is "+str(getStars(avg/count))
        return HttpResponse(res, status=200)
    else:
        return HttpResponse("No ratings available", status=404, content_type='text/plain')

#cross site scripting stop
@csrf_exempt
def viewratings(request):
    query = Rating.objects.all()
    #return all the records
    query = query.values("moduleInstance","professor","rating")
    dict = {}

    if(len(query)>0):
        for r in query:
            code=str(Professor.objects.get(id=r["professor"]).code)
            name=str(Professor.objects.get(id=r["professor"]).name)+ " ("+code+")"
            if(name not in dict):
                dict[name]=[r["rating"]]
            else:
                dict[name].append(r["rating"])

        jsonObject = {"ratings":dict}
        return HttpResponse(json.dumps(dict),status= 200)
    else:
        return HttpResponse("No ratings available",status= 404,content_type = 'text/plain')

#cross site scripting stop
@csrf_exempt
def moduleslist(request):
    query = ModuleInstance.objects.all()
    #return all the records
    query = query.values("module","professor","year","semester")

    jsonArray = []
    if(len(query)>0):
        for record in query:

            o = {"Code":str(Module.objects.get(id = record["module"]).code),
                "Name":str(Module.objects.get(id = record["module"]).module_name),
                "Year":record["year"],
                "Semester":record["semester"],
                "Taught_by":str(Professor.objects.get(id =record["professor"]))
                }
            jsonArray.append(o)

        #places json array into json object
        jsonObject = {"modules":jsonArray}
        return HttpResponse(json.dumps(jsonObject),status= 200)
    else:
        return HttpResponse("No modules available",status= 404,content_type = 'text/plain')
