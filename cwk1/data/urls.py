from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('moduleslist/',views.moduleslist,name = 'moduleslist'),
    path('getaverage/',views.getaverage,name = 'getaverage'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('register/', views.register, name='register'),
    path('rate/', views.rate, name='rate'),
    path('viewratings/',views.viewratings,name = 'viewratings')
]
