from django.contrib import admin
from .models import *

# Register your models here.
class ProfessorAdmin(admin.ModelAdmin):
    model = Professor
    list_display=('code','name','research_area')

class ModuleAdmin(admin.ModelAdmin):
    model = Module
    list_display = ('code','module_name','credits')

class ModuleInstanceAdmin(admin.ModelAdmin):
    model = ModuleInstance

    list_display = ('pk','module','professors','year','semester')

    def professors(self,obj):
        return obj.professor.all()


class RatingAdmin(admin.ModelAdmin):
    model = Rating
    list_display = ('moduleInstance','professor','rating')

    # def professors(self,obj):
    #     return obj.professor.all()


admin.site.register(Professor,ProfessorAdmin)
admin.site.register(Module,ModuleAdmin)
admin.site.register(ModuleInstance,ModuleInstanceAdmin)
admin.site.register(Rating,RatingAdmin)
