import requests
import json
import re
import math

class Client():

    def __init__(self):
        self.session = requests.Session()
        self.url = "http://sc16aht.pythonanywhere.com/api/"


    def startClient(self):
        while(True):
            #get input and get rid of spaces
            userinput = input()
            userinput = userinput.strip()
            userinput = userinput.split()
            if not userinput:
                continue
            elif(userinput[0] == 'list'):
                self.list_modules()
            elif(userinput[0] == 'view'):
                self.viewRatings()
            elif(userinput[0] == 'average' and len(userinput)==3):
                self.getAverage(userinput[1], userinput[2])
            elif(userinput[0]=='login'):
                self.login(self.url)
            elif(userinput[0]=="logout"):
                self.logout()
            elif(userinput[0]=="register"):
                self.register()
            elif(userinput[0]=="rate" and len(userinput)==6):
                self.rate(userinput[1], userinput[2],userinput[3], userinput[4], userinput[5])
            elif (userinput[0]=='exit'):
                break

    #rate professor_id module_code year semester rating     #need to be logged in first
    #COMP121        #KA1        #NA1  COMP3221
    #rate KA1 COMP121 2017 1 4
    def rate(self, pid, mcode, year, semester, rating):
        payload = {'pid': pid, 'mcode': mcode, "year": year, "semester":semester, "rating":rating}
        headers = {'content-type': 'application/x-www-form-urlencoded'}

        result = self.session.post(self.url+"rate/", data=payload, headers=headers, timeout=4)
        try:
            if(len(result.text) < 100):
                print(result.text)
            print("Status Code is %s" % result.status_code)
        except requests.exceptions.RequestException:
            print("Status Code is %s" % result.status_code)
            print("Error! Failed to do rating")


    def register(self):
        username=input("Enter Username: ")
        password=input("Enter Password: ")
        email=input("Enter Email: ")

        payload = {'username': username, 'password': password, "email":email }
        headers = {'content-type': 'application/x-www-form-urlencoded'}

        try:
            result = self.session.post(self.url+"register/", data=payload, headers=headers, timeout=4)
            print(result.text)
            print("Status Code is %s" % result.status_code)
        except requests.exceptions.RequestException:
            print("Error! Failed to register:")



    def logout(self):
        try:
            result = self.session.post(self.url+"logout/", timeout=4)
            print(result.text)
            print("Status Code is %s" % result.status_code)
        except requests.exceptions.RequestException:
            print("Error! Failed to logout")


    def login(self, url):
        username=input("Username: ")
        password=input("Password: ")

        payload = {'username': username, 'password': password}
        headers = {'content-type': 'application/x-www-form-urlencoded'}

        try:
            result = self.session.post(url+"login/", data=payload, headers=headers, timeout=4)
            print(result.text)
            print("Status Code is %s" % result.status_code)

        except requests.exceptions.RequestException:
            print("Error! Failed to connect to:", url)


    #COMP121        #KA1        #NA1  COMP3221
    def getAverage(self, pid, mcode):
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        payload = {'profId': pid, 'moduleCode': mcode}

        try:
            result = self.session.get(self.url+"getaverage", params=payload, headers=headers, timeout=4)
            if(result.status_code==200):
                print(str(result.text))

            print("\nstatus code %s" % result.status_code, "\n")
        except requests.exceptions.RequestException as e:
            print("error: "+e)

    def viewRatings(self):
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        try:
            result = self.session.get(self.url+"viewratings/",headers=headers,timeout=5)
            ratings = json.loads(result.text)
            print("\nstatus code %s" % result.status_code, "\n")

            for key in ratings:
                avg=0
                for r in ratings[key]:
                    avg+=r
                print("The rating of Professor ",key, " is ", self.getStars(avg/len(ratings[key])))

        except requests.exceptions.RequestException as e:
            print("error: "+e)


    def getStars(self, avg):
        avg=math.ceil(avg)
        stars=""
        for i in range(avg):
            stars+="*"

        return stars


    def list_modules(self):
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        try:
            result = self.session.get(self.url+"moduleslist/",headers=headers,timeout=5)
            modules = json.loads(result.text)
            print("\nstatus code %s" % result.status_code, "\n")
            print("Code".ljust(10), "Name".ljust(30), "Year".ljust(10), "Semester".ljust(10), "Taught by".ljust(30))

            prevCode=""
            prevyear=""
            prevsem=""
            for m in modules["modules"]:
                #print out the elements of the dictionary
                if(m["Code"]!=prevCode or prevyear!=m["Year"] or prevsem!=m["Semester"]):
                    print()
                    print(m["Code"].ljust(10), m["Name"].ljust(30), str(m["Year"]).ljust(10), str(m["Semester"]).ljust(10), m["Taught_by"].ljust(30))
                else:
                    print(" ".ljust(60), m["Taught_by"].ljust(30))

                prevCode=m["Code"]
                prevyear=m["Year"]
                prevsem=m["Semester"]
        except requests.exceptions.RequestException as e:
            print("error: "+str(e))


if __name__ == "__main__":
    client = Client()
    client.startClient()
